import subprocess
import os
import sys
import sentencegenerator
if (not os.path.exists("webhook.txt")):
	print("Put a Discord webhook URL in webhook.txt")
	sys.exit(1)
with open("webhook.txt", "r") as f:
	w = f.read()
	w = w.replace("\n", "")
s = sentencegenerator.SentenceGenerator()
sentence = s.generate()
sentence = sentence.replace("\"", "\\\"")
print(sentence)
os.system("curl -H \"Content-Type: application/json\" -X POST -d \"{\\\"content\\\":\\\"" + sentence + "\\\"}\" " + w)